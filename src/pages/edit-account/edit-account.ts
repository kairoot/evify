import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';
import { User } from '../../providers';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferError, FileTransferObject, FileUploadOptions, FileUploadResult } from '@ionic-native/file-transfer';


/**
 * Generated class for the EditAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-account',
  templateUrl: 'edit-account.html',
})
export class EditAccountPage {

  public account;
  public imageUri;
  public imageFileName;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewController: ViewController,
              public user: User,
              public toastController: ToastController,
              public camera: Camera,
              public ftf: FileTransfer
            ) {

          this.account = user._user;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditAccountPage');
  }

  closeModal()
  {
     this.viewController.dismiss();
  }

  changeProfilePicture()
  {
    
     let cameraOptions: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
     };

    //  console.log(this.camera);

     this.camera.getPicture()
     .then((imageData) => {
        this.imageUri = imageData;

        this.upload();
      })
     .catch(err => console.error(err));

     /* this.camera.getPicture(cameraOptions)
     .then((imageData) => {

        let base64Image = 'data:image/jpeg;base64,' + imageData;
        console.log(base64Image);
     }); */
   
  }

  upload()
  {
    const ftfProcess: FileTransferObject = this.ftf.create();

    let httpOptions = {
      // 'Content-Type':  'application/json',
      // 'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.user._token
    };

    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: 'image.jpg',
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      headers: httpOptions,
      httpMethod: "POST",
      // params: {'fileName': 'image.jpg'}
    };

    ftfProcess.upload(this.imageUri, this.user.api.url + '/user/picture', options)
    .then((data:any) => {

      // Getting the result string
      let response = JSON.parse( data.response );
      // console.log(response.message);
    

      // let result = JSON.parse(data);
      let toast = this.toastController.create({message: response.message, duration:3000});
      toast.present();
      // console.log(data);
      this.user._user.profile_pic = response.profile_pic;
    })
    .catch(err => console.error(err));
  }

  saveInformation()
  {
    this.user.updateAccount(this.account)
    .then((res: any) => {
      let data = JSON.parse( res.data );
      let toast = this.toastController.create({ message: data.message , duration: 3000});
      toast.present();
      this.user._user = data.update_user_account;
    })
    .catch(err => console.error(err));
  }
}
