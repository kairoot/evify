import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ToastOptions, MenuController } from 'ionic-angular';
import { Api, User } from '../../providers/index';
import { UiStatesProvider } from '../../providers/ui-states/ui-states';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public account:any = {
    email: "kairoot@yopmail.com",
    password: "password"
    // email: "",
    // password: ""
  }
  
  constructor(
                public navCtrl: NavController, 
                public navParams: NavParams,
                public user: User,
                public loadingController: LoadingController,
                public toastCtrl: ToastController,
                public menuCtrl: MenuController,
                public uiStateProvider: UiStatesProvider
              ) {
     uiStateProvider.disableSidebar();
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad LoginPage');
    
  }

  ionViewDidLeave() 
  {
   
  }

  goSignUp() 
  {
    this.navCtrl.setRoot('SignupPage');
  }

  doLogin()
  {

    let loader = this.loadingController.create({
      content:'Checking your credentials . . .'
    });

    let toastOptions: ToastOptions = {
      message: '',
      position: 'top',
      duration: 3000
    };

    let toast = this.toastCtrl.create(toastOptions);

    loader.present();

    this.user.login( this.account )
    .then((res:any) => {
      
        let data = JSON.parse( res.data ); 
        // data = data.result.data;
        console.log(data);
        loader.dismiss();

        if(data.status == 'success') 
        {
          console.log('SUCCESS IN SIGNING IN!');
          this.user._loggedIn( data.user , data.token);
          this.navCtrl.setRoot('TabsPage');
          this.uiStateProvider.enableSidebar();
        } 
    }) 
    .catch(err => {
      console.error(err);
      let res = JSON.parse(err.error);

      loader.dismiss();
      toast.setMessage(res.message);
      toast.present();
    });
  }

}
