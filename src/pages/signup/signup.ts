import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Toast , ToastOptions, MenuController} from 'ionic-angular';
import { User } from '../../providers';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { UiStatesProvider } from '../../providers/ui-states/ui-states';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

    public account:any = {
      email:"",
      password:"",
      firstname:"",
      lastname:"" , 
      contact_number: "",
      code:"",
      profile_pic: "",
      user_type:""
    };

    constructor(
                  public navCtrl: NavController, 
                  public navParams: NavParams,
                  public user: User,
                  public loadingCtrl: LoadingController,
                  public toastCtrl: ToastController,
                  private iab: InAppBrowser,
                  public menuCtrl: MenuController,
                  public uiStateProvider: UiStatesProvider
              ) {
      this.uiStateProvider.disableSidebar();
    }

    ionViewDidLoad() 
    {
      console.log('ionViewDidLoad LoginPage');
    }

    ionViewDidLeave() 
    {
    }

    goToLogin() 
    {
      this.navCtrl.setRoot('LoginPage'); 
    }

    doSignUp() 
    {   
        console.log(this.account);
        
        let loader = this.loadingCtrl.create({
          content: 'Please wait ...'
        });

        let toastOptions: ToastOptions = {
          message: '',
          position: 'top',
          duration: 3000
        };

        let toast = this.toastCtrl.create(toastOptions);

        loader.present(); 

        this.user.signup(this.account)
        .then((res: any) => {
          
          // Parsing the json data response of the server.
          let data = JSON.parse( res.data );
          // data = data.result.data;
          // console.log(data);
          // console.log(data.result.data);
          // console.log(data.result.data.user);
          // console.log(data.result.data.message);
          // console.log(data.result.data.status);
          
          if(data.status == 'success') {
            console.log('SUCCESS IN SIGNING UP!');
            toast.setMessage(data.message);
            this.user._loggedIn( data.user , data.token);
            this.uiStateProvider.enableSidebar();
            this.navCtrl.setRoot('TabsPage');
          } else {
            toast.setMessage(data.message);
          }

          toast.present();

          // console.log(data.url);
          // console.log(result);
          loader.dismiss();
        }) 
        .catch(err => {
          console.error('ERROR : ', err);
        })
    }

    triggerWebView()
    {
      let target_url = 'http://developer.globelabs.com.ph/dialog/oauth/EeX5IAXxGEu67ce96gTxxzu8oeEkILE4/';
      let browser = this.iab.create(target_url, '_self');
    }
}
