import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertOptions, AlertController, ModalController, ToastController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { SubscriptionProvider } from '../../providers/subscription/subscription';
import { User, Event } from '../../providers';

/**
 * Generated class for the UserAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-account',
  templateUrl: 'user-account.html',
})
export class UserAccountPage {

  public account:any;
  public posts:any = [];
  public subscribers = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public postController: PostProvider,
    public subsController: SubscriptionProvider,
    public user: User,
    public alertController: AlertController,
    public modalController: ModalController,
    public eventController: Event,
    public toastController: ToastController
    ) 
  {
      this.account = this.user._user;
      /*   
        postController.getUserPosts(this.account.id)
        .then((res: any) => {
          let data = JSON.parse(res.data);
          this.posts = data.posts;
          this.subscribers = data.subscribers_count;
        });
       */  
      this.eventController.getEventsByUser();
  }

  ionViewDidLoad() {
    
  }

  viewPost(item)
  {
    this.navCtrl.push("ViewPostPage", { post: item });
  }

  goLive() {

    let postFields = { title : "", description: ""};

    let alertOptions: AlertOptions = {
      title: 'Create Live Video',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancelled');
          }
        },
        {
          text: 'Go Live!',
          handler: data => {
            // this.post.createPost(data);
            this.navCtrl.push('BroadcasterPage', {'postFields' : data});
          }
        }
      ]
    };

    let alert = this.alertController.create(alertOptions);
 
    alert.present();
  }

  showEditAccountModal()
  {
    let modal = this.modalController.create('EditAccountPage');

    modal.present();
  }

  showEventFormModal() 
  {
      let modal = this.modalController.create('EventFormModalPage')

      modal.present();
  }

  viewEvent(item: any)
  { 
    // console.log(item);
    let modal = this.modalController.create('ViewEventPage', { item : item });
    modal.present();
    // this.navCtrl.push('ViewEventPage', { item : item });
  }

  deleteEvent(item: any) 
  {
    let toast = this.toastController.create({message: 'Success in deleting event #' + item.id, duration: 3000});

    this.eventController.deleteEvent(item.id)
    .then((response: any) => {
      console.log(response);
      toast.present();
      this.eventController.getEventsByUser();
    })
    .catch(err => console.error(err));
  }

  editEvent(item: any) 
  {
    let modal = this.modalController.create('EditEventFormModalPage', {data: item});
    modal.present();
  }
}
