import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionListModalPage } from './subscription-list-modal';

@NgModule({
  declarations: [
    SubscriptionListModalPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionListModalPage),
  ],
})
export class SubscriptionListModalPageModule {}
