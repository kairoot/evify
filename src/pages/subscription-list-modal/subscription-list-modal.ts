import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { SubscriptionProvider } from '../../providers/subscription/subscription';

/**
 * Generated class for the SubscriptionListModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscription-list-modal',
  templateUrl: 'subscription-list-modal.html',
})
export class SubscriptionListModalPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public subscriptionController: SubscriptionProvider,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public modalController: ModalController
  )
  {

  }

  ionViewDidLoad() {
    
  }
  
  closeModal()
  {
      this.viewCtrl.dismiss();
  }

  showAccountModal(item: any)
  {
    let modal = this.modalController.create('AccountChannnelPage', {'user' : item});
    modal.present();
  }

  unsubscribe(item: any)
  {
    // console.log(item);
    let loading = this.loadingController.create({content: 'Unsubscribing ...'});
    let toast = this.toastController.create({message: 'Success in unsubscribing to ' + item.firstname + ' ' + item.lastname, duration:3000});
    loading.present();

    this.subscriptionController.unsubscribe({ channel_id: item.id })
    .then((response: any) => {
      console.log(response);
      this.subscriptionController.getUserSubscriptsions();
      loading.dismiss();
      toast.present();
    })
    .catch( err => console.error(err));
  }
}
