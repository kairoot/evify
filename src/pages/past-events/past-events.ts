import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ToastController, AlertOptions } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { Event, User } from '../../providers';

/**
 * Generated class for the PastEventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-past-events',
  templateUrl: 'past-events.html',
})
export class PastEventsPage {
  public placeHolderImgs = [ 'alien.png', 'dinosaur.png', 'dog.jpg' , 'panda.png' ];

  public searchKey = "";


  constructor( 
                public navCtrl: NavController, 
                public navParams: NavParams,
                public post: PostProvider,
                public modalController: ModalController,
                public alertController: AlertController,
                public toastController: ToastController,
                public event: Event,
                public user: User
              ) {

        
  }

  ionViewDidLoad() {
    
  }

  public getRandomImage()
  {
     
  }

  /* 
    viewEvent( item: any ) {
      this.navCtrl.push('EventDetailsPage', { 'event': item });
    }

    showAddEventModal() {
      let modal = this.modalController.create('AddEventPage');

      modal.present();
    }

    goLive( item: any ) {
      this.navCtrl.push('BroadcasterPage', {'event' : item});
    }
  */

  search( event )
  {
    // let toast = this.toastController.create({message: this.searchKey})
    // toast.present();

    this.post.search({'key': this.searchKey})
    .then((result: any) => {
        console.log(result);
    })
    .catch(err => console.error(err));
  }

  doRefresh(event) {  
    console.log('Begin async operation');
    this.event.getEvents();
    // this.event.getEventsByUser();
    this.post.getPosts();
    setTimeout(() => {
      console.log('Async operation has ended');
      // event.target.complete();
      event.complete();
    }, 2000);
  }

  viewPost(item)
  {
    // this.navCtrl.push()
    this.navCtrl.push("ViewPostPage", { post: item });
    // this.navCtrl.setRoot("ViewPostPage", { post: item });

    // let viewPostModal = this.modalController.create("ViewPostPage", { post: item });
    // viewPostModal.present();
  }

  goLive() {

    let postFields = { title : "", description: ""};

    let alertOptions: AlertOptions = {
      title: 'Create Live Video',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancelled');
          }
        },
        {
          text: 'Go Live!',
          handler: data => {
            // this.post.createPost(data);
            this.navCtrl.push('BroadcasterPage', {'postFields' : data});
          }
        }
      ]
    };

    let alert = this.alertController.create(alertOptions);

    alert.present();
  }

  viewAccount(item)
  {
    let viewAccountModal = this.modalController.create("AccountChannnelPage", { user: item });
    viewAccountModal.present();
  }

  viewEvent(item)
  {
    let modal = this.modalController.create('ViewEventPage', { item : item });

    modal.present();
  }

  onInput($event)
  {

  }

  onCancel($event)
  {
    
  }

  registerToEvent(item : any)
  {
    // Register to an event
    item.is_registered = true;
    item.interested_count++; 
    
    this.event.register(item.id)
    .then((response: any) => {
      let data = JSON.parse( response.data );
      console.log(data);
    })
    .catch(err => console.error(err));
  }

  unregisterToEvent(item : any)
  {
    // Unregister to an event
    item.is_registered = false;
    item.interested_count--; 
    this.event.unregister(item.id)
    .then((response: any) => {
      let data = JSON.parse( response.data );
      console.log(data);
    })
    .catch(err => console.error(err));
   
  }

  showEventFormModal() 
  {
      let modal = this.modalController.create('EventFormModalPage')

      modal.present();
  }

}
