import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, AlertOptions, ModalController } from 'ionic-angular';
import { User } from '../../providers';
import { PageInfoProvider } from '../../providers/page-info/page-info';

/**
 * Generated class for the SubscriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscriptions',
  templateUrl: 'subscriptions.html',
})
export class SubscriptionsPage {

  public subscriptions:any = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertController: AlertController,
              public userProvider: User,
              public modalController: ModalController,
              public pageInfo: PageInfoProvider
             ) {
    this.userProvider.myAccount()
    .then((res:any) => {
      let data = JSON.parse( res.data );
      this.subscriptions = data.subscriptions;
      console.log(this.subscriptions);
      
    })
    .catch(err => console.error());
  }

  ionViewDidLoad() {
    
  }

  goLive() {

    let postFields = { title : "", description: ""};

    let alertOptions: AlertOptions = {
      title: 'Create Live Video',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancelled');
          }
        },
        {
          text: 'Go Live!',
          handler: data => {
            // this.post.createPost(data);
            this.navCtrl.push('BroadcasterPage', {'postFields' : data});
          }
        }
      ]
    };

    let alert = this.alertController.create(alertOptions);

    alert.present();
  }

  viewAccount(item)
  {
    let viewAccountModal = this.modalController.create("AccountChannnelPage", { user: item });
    viewAccountModal.present();
  }

  viewPost(item)
  {
    this.navCtrl.push("ViewPostPage", { post: item });
  }
}
