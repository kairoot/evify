import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { User, Event } from '../../providers';

/**
 * Generated class for the AddEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-event',
  templateUrl: 'add-event.html',
})
export class AddEventPage {


  public event:any = {
    user_id : "",
    title : "", 
    description : "", 
    banner : "dummy.jpg",
    status : "pending"
  };



  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewController: ViewController,
              public user: User,
              public eventController: Event,
              public loadingController: LoadingController,
              public toastController: ToastController
              ) {

      this.event.user_id = user._user.id;
      // this.event.user_id = 1;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddEventPage');
  }

  dismissModal() {
    this.viewController.dismiss();
  }

  saveEvent() {

    let loader = this.loadingController.create({
      content:'Adding your event'
    });

    let toast = this.toastController.create({duration: 2000});

    loader.present();

    this.eventController.addEvent( this.event )
    .then((res: any) => {
      let data = JSON.parse( res.data );
      data = data.result.data;
      toast.setMessage(data.message);
      

      this.eventController.getEventsByUser();
      toast.present();
      loader.dismiss();
      this.dismissModal();
    })
    .catch(err => console.error(err));

  }






}
