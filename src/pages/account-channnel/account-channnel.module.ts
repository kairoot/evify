import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountChannnelPage } from './account-channnel';

@NgModule({
  declarations: [
    AccountChannnelPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountChannnelPage),
  ],
})
export class AccountChannnelPageModule {}
