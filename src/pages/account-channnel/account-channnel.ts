import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertOptions, AlertController } from 'ionic-angular';
import { PostProvider } from '../../providers/post/post';
import { SubscriptionProvider } from '../../providers/subscription/subscription';

/**
 * Generated class for the AccountChannnelPage page.
 * 
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
``
@IonicPage()
@Component({
  selector: 'page-account-channnel',
  templateUrl: 'account-channnel.html',
})
export class AccountChannnelPage {

  public account:any;
  public posts:any = [];
  public subscribed:boolean;
  public subscribers = 0;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewController: ViewController,
              public postController: PostProvider,
              public subsController: SubscriptionProvider,
              public toastController: ToastController,
              public alertController: AlertController
             ) 
  {
    this.account = navParams.get('user');
    this.init();
  }

  ionViewDidLoad() 
  {
    
  }

  closeModal()
  {
     this.viewController.dismiss();
  }

  viewPost(item)
  {
    this.navCtrl.push("ViewPostPage", { post: item });
  }

  public init()
  {
    this.postController.getUserPosts(this.account.id)
    .then((res: any) => {
      let data = JSON.parse(res.data);
      this.posts = data.posts;
      this.subscribed = data.subscribed;
      this.subscribers = data.subscribers_count;
    });
  }

  public subscribe()
  {
    let toast = this.toastController.create({message: 'You are subscribe now!', duration: 3000});

    this.subsController.subscribe({channel_id: this.account.id})
    .then((res: any) => {
      console.log(res);
      console.log('You are subscribed now!');
      this.subscribed = true;
      this.init();
      this.subsController.getUserSubscriptsions();
      toast.present();
    }) 
    .catch(err => console.error(err));
  
  }

  public unsubscribe()
  {
    let toast = this.toastController.create({message: 'You are unsubscribed now!', duration: 3000});

    this.subsController.unsubscribe({channel_id: this.account.id})
    .then((res: any) => {
      console.log(res);
      console.log('You are unsubscribed now!');
      this.subscribed = false;
      this.init();
      this.subsController.getUserSubscriptsions();
      toast.present();
    })
    .catch(err => console.error(err));
  }

  goLive() {

    let postFields = { title : "", description: ""};

    let alertOptions: AlertOptions = {
      title: 'Create Live Video',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancelled');
          }
        },
        {
          text: 'Go Live!',
          handler: data => {
            // this.post.createPost(data);
            this.navCtrl.push('BroadcasterPage', {'postFields' : data});
          }
        }
      ]
    };

    let alert = this.alertController.create(alertOptions);

    alert.present();
  }
}
