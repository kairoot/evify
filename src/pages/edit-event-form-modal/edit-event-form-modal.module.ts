import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditEventFormModalPage } from './edit-event-form-modal';

@NgModule({
  declarations: [
    EditEventFormModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditEventFormModalPage),
  ],
})
export class EditEventFormModalPageModule {}
