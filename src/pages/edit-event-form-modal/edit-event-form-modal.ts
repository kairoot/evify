import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';
import { Event, User } from '../../providers';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferError, FileTransferObject, FileUploadOptions, FileUploadResult } from '@ionic-native/file-transfer';
/**
 * Generated class for the EditEventFormModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-event-form-modal',
  templateUrl: 'edit-event-form-modal.html',
})
export class EditEventFormModalPage {

  
  public event;

  public imageUri;
  public imageFileName;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewController: ViewController,
              public datePicker: DatePicker,
              public eventProvider: Event,
              public toastController: ToastController,
              public camera: Camera,
              public ftf: FileTransfer,
              public user: User,
              public loadingController: LoadingController
              ) {
      this.event = navParams.get('data');
  }

  ionViewDidLoad() 
  {

  }

  closeModal()
  {
    this.viewController.dismiss();
  }

  saveEvent()
  {
     console.log(this.event);
     let toast = this.toastController.create({
       message: 'Success in updating event records.',
       duration: 3000
     });

     this.eventProvider.updateEvent(this.event)
     .then((response:any) => {
        this.eventProvider.getEvents();
        toast.present();
        this.eventProvider.getEventsByUser();
        this.viewController.dismiss();
     })
     .catch(err => console.error(err));
  }

  selectDate(dateType:any)
  {
     let options:DatePickerOptions = {
        allowOldDates:false,
        date: new Date(),
        mode: 'datetime',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
        titleText: (dateType == 'startDate') ? 'Select Start Date' : 'Select End Date'
     }


     this.datePicker.show(options)
     .then((result: Date) => {
        
      if(dateType == 'startDate')
        this.event.start_date_time = result.toUTCString();
      else 
        this.event.end_date_time = result.toUTCString();
     })
     .catch(err => console.error(err));
     
  }

  changeEventBanner()
  {
    let cameraOptions: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
     };

     this.camera.getPicture()
     .then((imageData) => {
        this.imageUri = imageData;
        this.upload();
        console.log(this.imageUri);
      })
     .catch(err => console.error(err));
  }

  upload()
  {
    const ftfProcess: FileTransferObject = this.ftf.create();

    let httpOptions = {
      // 'Content-Type':  'application/json',
      // 'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.user._token
    };

    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: 'image.jpg',
      chunkedMode: false,
      mimeType: 'multipart/form-data',
      headers: httpOptions,
      httpMethod: "POST",
      params: {'id': this.event.id}
    };

    let loader = this.loadingController.create({
                    content: 'Uploading banner',
                 });

    loader.present();

    ftfProcess.upload(this.imageUri, this.user.api.url + '/event/picture', options)
    .then((data:any) => {

      // Getting the result string
      let response = JSON.parse( data.response );
      // console.log(response.message);
    
      loader.dismiss();
      // let result = JSON.parse(data);
      let toast = this.toastController.create({message: 'Success in setting banner', duration:3000});
      toast.present();
      console.log(data);
      this.event.banner = response.banner;
    })
    .catch(err => console.error(err));
  }

}
