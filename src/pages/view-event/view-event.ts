import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertOptions, AlertController } from 'ionic-angular';
import { Event, User } from '../../providers';

/**
 * Generated class for the ViewEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-event',
  templateUrl: 'view-event.html',
})
export class ViewEventPage {

  public event;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewController: ViewController,
              public eventController: Event,
              public user: User,
              public alertController: AlertController
             ) 
  {
    this.event = navParams.get('item');
  }
 

  ionViewDidLoad() {
     
  }

  closeModal()
  {
    this.viewController.dismiss();
  }

  registerToEvent()
  {
    // Register to an event
    this.event.is_registered = true;
    this.event.interested_count++; 
    
    this.eventController.register(this.event.id)
    .then((response: any) => {
      let data = JSON.parse( response.data );
      console.log(data);
    })
    .catch(err => console.error(err));
  }

  unregisterToEvent(item : any)
  {
    // Unregister to an event
    this.event.is_registered = false;
    this.event.interested_count--; 

    this.eventController.unregister(this.event.id)
    .then((response: any) => {
      let data = JSON.parse( response.data );
      console.log(data);
    })
    .catch(err => console.error(err));
   
  } 


  goLive() {

    let postFields = { title : "", description: ""};

    let alertOptions: AlertOptions = {
      title: 'Create Live Video',
      inputs: [
        {
          name: 'title',
          placeholder: 'Title',
          type: 'text'
        },
        {
          name: 'description',
          placeholder: 'Description',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancelled');
          }
        },
        {
          text: 'Go Live!',
          handler: data => {
            // this.post.createPost(data);
            data.event_id = this.event.id;
            this.navCtrl.push('BroadcasterPage', {'postFields' : data});
            // this.viewController.dismiss();
          }
        }
      ]
    };

    let alert = this.alertController.create(alertOptions);

    alert.present();
  }

  viewPost(item : any)
  {
    this.navCtrl.push('ViewPostPage', { post : item});
  }
}
