import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { DatePicker, DatePickerOptions } from '@ionic-native/date-picker';
import { Event } from '../../providers';


/**
 * Generated class for the EventFormModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-form-modal',
  templateUrl: 'event-form-modal.html',
})
export class EventFormModalPage {


  public event = {
    title: "",
    description: "",
    location : "", 
    start_date_time: "2019-03-30 00:00:00",
    end_date_time: "2019-03-30 00:00:00",
    banner: ""
  };

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewController: ViewController,
              public datePicker: DatePicker,
              public eventProvider: Event,
              public toastController: ToastController
              ) {
  }

  ionViewDidLoad() 
  {

  }

  closeModal()
  {
    this.viewController.dismiss();
  }

  saveEvent()
  {
     console.log(this.event);
     let toast = this.toastController.create({
       message: 'Success in adding a new event',
       duration: 3000
     });

     this.eventProvider.addEvent(this.event)
     .then((response:any) => {
        // console.log(JSON.parse(response));
        this.eventProvider.getEvents();
        toast.present();
        this.eventProvider.getEventsByUser();
        this.viewController.dismiss();
     })
     .catch(err => console.error(err));
  }

  selectDate(dateType:any)
  {
     let options:DatePickerOptions = {
        allowOldDates:false,
        date: new Date(),
        mode: 'datetime',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
        titleText: (dateType == 'startDate') ? 'Select Start Date' : 'Select End Date'
     }


     this.datePicker.show(options)
     .then((result: Date) => {
        
      if(dateType == 'startDate')
        this.event.start_date_time = result.toUTCString();
      else 
        this.event.end_date_time = result.toUTCString();
     })
     .catch(err => console.error(err));
     
  }

}
