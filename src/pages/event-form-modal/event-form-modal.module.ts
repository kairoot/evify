import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventFormModalPage } from './event-form-modal';

@NgModule({
  declarations: [
    EventFormModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EventFormModalPage),
  ],
})
export class EventFormModalPageModule {}
