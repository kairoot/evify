import { Component } from '@angular/core';
import { Platform, ToastController, IonicPage, NavController, NavParams, AlertController, AlertOptions } from 'ionic-angular';
import { Event } from '../../providers';
import { PostProvider } from '../../providers/post/post';

// Application id generated at https://dashboard.bambuser.com/developer
// const APPLICATION_ID:string = 'Qz7zryPtqNRT8BxfaEVENg';
const APPLICATION_ID:string = '4u9LPoNH4fpb7IYyQrhMcw';

@IonicPage()
@Component({
  selector: 'page-broadcaster',
  templateUrl: 'broadcaster.html'
})

export class BroadcasterPage {
  isBroadcasting = false;
  isPending = false;
  broadcaster: any;
  errListenerId = false;

  newPost:any;
 
  constructor(
    private toastCtrl: ToastController,
    public platform: Platform,
    public navController: NavController,
    public navParams: NavParams,
    public postController: PostProvider,
    public alertController: AlertController,
    public eventController: Event
  ) {
 
    this.newPost = navParams.get('postFields');
    console.log('PASSED POST: ',this.newPost);

    // this.postController.createPost(this.newPost);

    platform.ready().then(() => {
      // Using array syntax workaround, since types are not declared.
      if (window['bambuser']) {
        this.broadcaster = window['bambuser']['broadcaster'];
        this.broadcaster.setApplicationId(APPLICATION_ID);
      } else {
        // Cordova plugin not installed or running in a web browser
      }
    });
  }

  async ionViewDidEnter() {
    if (APPLICATION_ID.endsWith('NGEME')) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('Warning: APPLICATION_ID is not set. Get your application id at https://dashboard.bambuser.com/developer and update pages/broadcaster/broadcaster.ts, then rebuild the app.');
    }

    // Engage our Ionic CSS background overrides that ensure viewfinder is visible.
    document.getElementsByTagName('body')[0].classList.add("show-viewfinder");

    // document.getElementsByTagName('html')[0].style.backgroundColor = 'transparent !important';

    if (!this.broadcaster) {
      await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish before using alert()
      alert('broadcaster is not ready yet');
      return;
    }

    console.log('Starting viewfinder');
    this.broadcaster.showViewfinderBehindWebView();
  }

  ionViewWillLeave() {
    // Disengage our Ionic CSS background overrides, to ensure the rest of the app looks ok.
    document.getElementsByTagName('body')[0].classList.remove("show-viewfinder");
    // document.getElementsByTagName('html')[0].style.backgroundColor = '#fff !important';
    console.log('Removing viewfinder');
    if (this.broadcaster) {
      this.broadcaster.hideViewfinder();
    }
  } 

  async start() {
    if (this.isBroadcasting || this.isPending) return;

    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Starting broadcast...',
      position: 'middle',
    });
    await toast.present();

    console.log('Starting broadcast');
    try {
      
      this.postController.createPost(this.newPost)
      .then((res:any) => {
        let data = JSON.parse( res.data );
        // console.log(data.data);
        this.newPost = data.data.post;
        console.log('BROADCAST INFO: ', this.newPost);

        this.triggerBroadCast().then((res: any) => {

          /* 
            this.postController.forceUpdate(this.newPost.id)
            .then((res:any) => {
              console.log(res);
            }).catch(err => console.error(err));
          */
         console.log('Now creating the broadcast.');
        })
        .catch(err => console.error(err));
      })
      .catch(err => console.error(err));

      toast.dismiss();
      this.isBroadcasting = true;
      this.isPending = false;
      this.listenForError();

    } catch (e) {
      toast.dismiss();
      this.isPending = false;
      alert('Failed to start broadcast');
      console.log(e);   
    }
  }

  async triggerBroadCast() {
    await this.broadcaster.startBroadcast();  
    await new Promise(resolve => setTimeout(resolve, 1000));
  }

  async stop() {
    if (!this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Ending broadcast...',
      position: 'middle'
    });
    await toast.present();

    console.log('Ending broadcast');
    try {
      await this.broadcaster.stopBroadcast();
      toast.dismiss();
      this.isBroadcasting = false;
      this.isPending = false;
      
      this.postController.endLiveSession(this.newPost.id)
      .then((res:any) => {
        console.log(res);
        toast.dismiss();
        this.isBroadcasting = false;
        this.isPending = false;
        this.eventController.getEvents();
        
      })
      .catch(err => console.log(err));

    } catch (e) {
      toast.dismiss();
      this.isPending = false;
      alert('Failed to stop broadcast');
      console.log(e);
    }
  }

  listenForError() {
    if (this.errListenerId) return;
    this.errListenerId = this.broadcaster.addEventListener('connectionError', status => {
      this.isBroadcasting = false;
      this.isPending = false;
      const toast = this.toastCtrl.create({
        message: 'Connection error',
        position: 'middle',
        duration: 3000,
      });
      toast.present();
    });
  }

  switchCamera() {
    if (this.broadcaster) {
      this.broadcaster.switchCamera();
    }
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  } 
}
