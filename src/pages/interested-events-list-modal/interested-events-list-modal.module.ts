import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InterestedEventsListModalPage } from './interested-events-list-modal';

@NgModule({
  declarations: [
    InterestedEventsListModalPage,
  ],
  imports: [
    IonicPageModule.forChild(InterestedEventsListModalPage),
  ],
})
export class InterestedEventsListModalPageModule {}
