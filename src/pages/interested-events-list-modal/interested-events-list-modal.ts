import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { Event, User } from '../../providers';

/**
 * Generated class for the InterestedEventsListModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-interested-events-list-modal',
  templateUrl: 'interested-events-list-modal.html',
})
export class InterestedEventsListModalPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public eventCtrl: Event,
    public userCtrl: User,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController
  )
  {
    this.eventCtrl.getUserRegisteredEvents(this.userCtrl._user.id);
  }

  ionViewDidLoad() 
  {
    
  }

  closeModal()
  {
    this.viewCtrl.dismiss();
  }

  cancelRegistration(item: any)
  {
    // console.log(item);
    let loading = this.loadingCtrl.create({content: 'Cancelling event registration...'});
    let toast = this.toastCtrl.create({message: 'Success in cancelling event #' + item.id, duration: 3000});

    loading.present();

    this.eventCtrl.unregister(item.id)
    .then((response: any) => {
      console.log(response);
      this.eventCtrl.getUserRegisteredEvents(this.userCtrl._user.id);
      loading.dismiss();
      toast.present();
    })
    .catch(err => console.error(err));
  }

  viewEvent(item: any)
  {
    console.log(item);
    let modal = this.modalCtrl.create('ViewEventPage', { item: item });
    modal.present();
  }
}
