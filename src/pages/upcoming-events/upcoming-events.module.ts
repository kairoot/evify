import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpcomingEventsPage } from './upcoming-events';

@NgModule({
  declarations: [
    UpcomingEventsPage,
  ],
  imports: [
    IonicPageModule.forChild(UpcomingEventsPage),
  ],
})
export class UpcomingEventsPageModule {}
