import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateEventFormPage } from './update-event-form';

@NgModule({
  declarations: [
    UpdateEventFormPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateEventFormPage),
  ],
})
export class UpdateEventFormPageModule {}
