import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import { User } from '../user/user'


const httpOptions = {
  'Content-Type':  'application/json',
  'Accept': 'application/json',
  'Authorization':'Bearer '
};


@Injectable()
export class PostProvider {

  public _posts:any = [];
  

  constructor(
              public api: Api,
              public user: User
             ) {
    httpOptions.Authorization += user._token;
    
    this.getPosts();
  }

  public posts() {
    return this._posts;
  }

  public getPosts( id = null) {
    
    if(id == null)
    {
      this.api.get('posts', {}, httpOptions)
      .then((res:any) => {
        let data = JSON.parse( res.data );
        this._posts = data.posts;
      })  
      .catch(err => console.error(err));
    }
    else 
    { 
     
    }
    
  }

  public getUserPosts(id)
  {
      return this.api.get('user/' + id, {}, httpOptions);
      // .then((res: any) => {
      //   let data = JSON.parse(res.data);

      //   //  console.log(data);
      //   // console.log( data.posts );

      //   // return new Promise((resolve, reject) => {
      //   //   resolve(data.posts);
      //   // });
      //   // return Promise.resolve(data.posts);
      // })
      // .catch(err => console.error(err));
  }

  public createPost(post: any)
  {
    console.log('CREATING A NEW POST: ', post);

    return this.api.post('posts', post, httpOptions);
    // .then((res:any) => {

    // })
    // .catch(err => console.error(err));
  }

  public endLiveSession(id)
  {
    console.log('ENDING SESSION');

    return this.api.get('post/end-live/' + id, {}, httpOptions);
  }

  public forceUpdate(id)
  {
    console.log('FORCING UPDATE');
    return this.api.get('post/force-update/' + id, {}, httpOptions);
  }

  public search(searchKey: any) 
  {
    return this.api.post('search', searchKey, httpOptions)
    // .then((result: any) => {
    //   console.log(result);
    // })
    // .catch(err => console.error(err));
  }
}
