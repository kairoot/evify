import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import { User } from '../user/user';


const httpOptions = {
  'Content-Type':  'application/json',
  'Accept': 'application/json'
};

@Injectable()
export class Event {

  public _httpOpt = {
    'Content-Type':  'application/json',
    'Accept': 'application/json',
    'Authorization' : ''
  };

  public _events:any = [];

  public _user_events: any = [];

  public _user_registered_events:any = [];

  constructor(
                public api:Api,
                public user: User
             ) {
      
      // this.getEventsByUser();
      this._httpOpt.Authorization = 'Bearer ' + this.user._token;
      this.getEvents();
      this.getUserRegisteredEvents(this.user._user.id);
  }

  public events() {
    return this._events;
  }

  public getEvents() {

    this.api.get('events', {}, this._httpOpt)
    .then((res:any) => {
      let data = JSON.parse( res.data );
      this._events = data;

      console.log(this._events);
    })  
    .catch(err => console.error(err));
  }

  public getEventsByUser() {
    this.api.get('user/' + this.user._user.id + '/events',{},this._httpOpt)
    .then((res:any) => {
      // console.log(res);
      let data = JSON.parse( res.data );
      this._user_events = data.events;
    })  
    .catch(err => console.error(err));
  }

  public addEvent(event: any) 
  {
    console.log(this._httpOpt);
    console.log(event);
    return this.api.post('events', event, this._httpOpt);
  }

  public sendLive(event: any) {
    return this.api.get('event/live/' + event.id, {}, httpOptions);
  }

  public endLive(event: any) {
    return this.api.get('event/end/' + event.id, {}, httpOptions);
  }

  public register(eventId: number)
  {
    return this.api.post('event/register', { event_id: eventId }, this._httpOpt);
  }

  public unregister(eventId: number)
  {
    return this.api.post('event/unregister', { event_id: eventId }, this._httpOpt);
  }

  public deleteEvent(eventId: number)
  {
    return this.api.get('delete/' + eventId + '/events' , { }, this._httpOpt);
  }

  public updateEvent(event: any)
  {
    return this.api.post('update/' + event.id + '/events', event, this._httpOpt);
  }
  
  public getUserRegisteredEvents(user_id: number)
  {
    this.api.get('event/subs/' + user_id, { }, this._httpOpt)
    .then(( response: any) => {
      let data = JSON.parse(response.data);
      this._user_registered_events = data.events;
      console.log(this._user_registered_events);
    })
    .catch(err => console.error(err));
  }

}
