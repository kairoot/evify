import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PageInfoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PageInfoProvider {

  public _user:any;

  constructor(public http: HttpClient) {
    
  }

  public setUser(user)
  {
    this._user = user;
  }



}
