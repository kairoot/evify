import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HTTP } from '@ionic-native/http';
/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
    // url: string = 'https://e-travel-bicol.000webhostapp.com/api';
    // url: string = 'http://127.0.0.1:8000/api';
    url: string = 'https://postmefy.herokuapp.com/api';
    // url:string = 'localhost/caketify/api';
  constructor(public http: HttpClient, public httpNative: HTTP) { }

  get(endpoint: string, params?: any, reqOpts?: any) {
    // if (!reqOpts) {
    //   reqOpts = {
    //     params: new HttpParams()
    //   };
    // }

    // // Support easy query params for GET requests
    // if (params) {
    //   reqOpts.params = new HttpParams();
    //   for (let k in params) {
    //     reqOpts.params = reqOpts.params.set(k, params[k]);
    //   }
    // } 

    // return this.http.get(this.url + '/' + endpoint, reqOpts);
    return this.httpNative.get(this.url + '/' + endpoint, params, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    // return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    return this.httpNative.post(this.url + '/' + endpoint, body, reqOpts);
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    return this.http.put(this.url + '/' + endpoint, body, reqOpts);
  }

  delete(endpoint: string, reqOpts?: any) {
    return this.http.delete(this.url + '/' + endpoint, reqOpts);
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
  }
}
