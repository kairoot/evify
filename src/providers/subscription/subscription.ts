import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../api/api';
import { User } from '../user/user'

const httpOptions = {
  'Content-Type':  'application/json',
  'Accept': 'application/json',
  'Authorization':'Bearer '
};

@Injectable()
export class SubscriptionProvider {

  public channels;

  constructor(
    public api: Api,
    public user: User
   ) {
        httpOptions.Authorization += user._token;    
        this.getUserSubscriptsions();
  }

  public subscribe(data:any)
  {
    let params = {
      channel_id: data.channel_id,
      subscriber_id: this.user._user.id
    };

    return this.api.post('subscriptions', params, httpOptions);
  }

  public unsubscribe(data:any)
  {
    let params = {
      channel_id: data.channel_id,
      subscriber_id: this.user._user.id
    };

    return this.api.post('subscription/unsubscribe', params, httpOptions);
  }

  public getUserSubscriptsions()
  {
    this.api.get('user/subs/'+  this.user._user.id,{},httpOptions)
    .then((response: any) => {
      let data = JSON.parse(response.data);

      // displays the user subscribed to channels
      this.channels = data.channels;
      console.table(this.channels);
      
    })
    .catch(err => console.error(err));
  }

}
