import 'rxjs/add/operator/toPromise';

import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';

import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 * 
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */


const httpOptions = {
    'Content-Type':  'application/json',
    'Accept': 'application/json'
  };

@Injectable()
export class User { 
  _user: any;
  _token: any;

  constructor(public api: Api) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) 
  {  
    return this.api.post('login',accountInfo,httpOptions);
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) 
  {
    return this.api.post('register', accountInfo, httpOptions);
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() 
  {
    this._user = null;
  }
 
  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(user, token) 
  {
    
    this._user = user;
    this._token = token;
  }

  myAccount() 
  {
    let httpOpt = { 
                    'Content-Type':  'application/json',
                    'Accept': 'application/json' ,
                    'Authorization': 'Bearer'
                  };
    
    httpOpt.Authorization += this._token;
    

    return this.api.get('user',{},httpOpt);
  }

  updateAccount(accountInfo: any)
  {
    let httpOpt = { 
      'Content-Type':  'application/json',
      'Accept': 'application/json' ,
      'Authorization': 'Bearer '
    };

    httpOpt.Authorization += this._token;

    return this.api.post('user/updateacc', accountInfo, httpOpt);
  }

}
