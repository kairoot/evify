import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UiStatesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UiStatesProvider {

  public is_menu_enabled = false;

  constructor(public http: HttpClient) 
  {
    
  }

  public enableSidebar() 
  {
    this.is_menu_enabled = true;
  }

  
  public disableSidebar() 
  {
    this.is_menu_enabled = false;
  }

}
