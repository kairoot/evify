import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Api } from '../providers/api/api';
import { User} from '../providers/user/user';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { HTTP  } from '@ionic-native/http';
import { Event } from '../providers/event/event';
import { PostProvider } from '../providers/post/post';
import { SubscriptionProvider } from '../providers/subscription/subscription';
import { PageInfoProvider } from '../providers/page-info/page-info';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { FileTransfer, FileTransferObject, FileUploadOptions  } from '@ionic-native/file-transfer';
import { DatePicker  } from '@ionic-native/date-picker';
import { UiStatesProvider } from '../providers/ui-states/ui-states';
// import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer/ngx';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [ 
    Camera,
    FileTransfer,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Api,
    User,
    HTTP,
    Event,
    PostProvider,
    SubscriptionProvider,
    PageInfoProvider,
    InAppBrowser,
    DatePicker,
    UiStatesProvider
  ]
})
export class AppModule {}
