import { Component, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Config, Nav, Platform, ModalController, ModalOptions, ViewController } from 'ionic-angular';

import { User } from '../providers';
import { UiStatesProvider } from '../providers/ui-states/ui-states';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = "LoginPage";

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Home', component: 'TabsPage' , type: 'tabs-page', index: 0 },
    // { title: 'Notifications', component: '' },
    // { title: 'Subscriptions', component: '' , type: 'tabs-page', index: 1},
  ]

  notifModal:any;

  constructor(platform: Platform, 
             statusBar: StatusBar, 
             splashScreen: SplashScreen,
             public user: User,
             public modal: ModalController,
             public uiProvider: UiStatesProvider
            //  public viewController: ViewController
             ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.notifModal = this.modal.create("NotificationsPage");
    });
  }

  logout() {
    this.user.logout();
    
    // this.packageProvider.clearPackageData();
    // this.transactionProvider.clearTransactionData();
    // this.itineraryProvider.clearItineraryData();
    this.openPage({ title:"Logout", component: "LoginPage" });
  }

  openPage(page:any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  showNotifModal() 
  {
    // let notifModal = this.modal.create("NotificationsPage");
    this.notifModal.present();
  }

  closeModal()
  {
    this.notifModal.viewController.dismiss();
  }

  showSubscriptionListModal()
  {
      let subModal = this.modal.create('SubscriptionListModalPage');
      subModal.present();
  }

  showEventListModal()
  {
      let subModal = this.modal.create('InterestedEventsListModalPage');
      subModal.present();
  }

}
